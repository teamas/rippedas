var randomWords = ["thewolf", "jefferson", "dumplings", "n2", "lyle", "dean", "stirfry", "icemilk", "sushi", "balz", "gooch"];
var daysOfTheWeek = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"]
var current = new Date();
var c_hours = current.getHours();
var c_min = current.getMinutes();

$(document).ready(function () {
	// On Document Ready
	console.log("Document Ready.");

	var theWord = randomWords[Math.floor(Math.random()*randomWords.length)]
	//$('title').append(theWord);
	$('#big_head').append(theWord);

	$('#open_list').html('');
	$.getJSON("js/schedule.json", function(data) {
		// data is a JavaScript object now. Handle it as such
		var featuresList = data.features;
		var halls = data.locations;
		console.log(featuresList);
		for(var i = 0; i < halls.length; i++) {
			// Check every hall.
			var the_hall = halls[i];
			var theDay = daysOfTheWeek[current.getDay()];
			var current_day = the_hall.hours[theDay];
			if (current_day == undefined) { continue; }
			var hours = current_day.open;
			var hallFeatures = [];
			for (var k = 0; k < featuresList.length; k++) {
				if(current_day[featuresList[k].name] != undefined) {
					var pushObject = {'name': featuresList[k], 'value': current_day[featuresList[k].name]};
					hallFeatures.push(pushObject);
				}
			}
			for(var j = 0; j < hours.length; j++) {
				var open = hours[j][0].split(":");
				var close = hours[j][1].split(":");
				if(open[0] < c_hours && close[0] > c_hours) {
					checkFeatures(the_hall, hallFeatures);
				} else if (open[0] == c_hours) {
					if (open[1] <= c_min) {
						checkFeatures(the_hall, hallFeatures);
					}
				} else if (close[0] == c_hours) {
					if (close[1] >= c_min) {
						checkFeatures(the_hall, hallFeatures);
					}
				}
			}
		}
		if($('#open_list').html() == "") {
			$('#open_list').html('<tr class="warning"><td>Nothing is open right now...</td></tr>');
		}
	});
});

function checkFeatures(the_hall, to_check) {
	var current_features = [];
	for(var k = 0; k < to_check.length; k++) {
		var checker = to_check[k].value;
		var pushCheck = to_check[k].name;
		if(checker == true) {
			current_features.push(pushCheck);
			continue;
		}
		for(var j = 0; j < checker.length; j++) {
			var open = checker[j][0].split(":");
			var close = checker[j][1].split(":");
			if(open[0] < c_hours && close[0] > c_hours) {
					current_features.push(pushCheck);
			} else if (open[0] == c_hours) {
				if (open[1] <= c_min) {
					current_features.push(pushCheck);
				}
			} else if (close[0] == c_hours) {
				if (close[1] >= c_min) {
					current_features.push(pushCheck);
				}
			}
		}
	}
	isOpen(the_hall, current_features)
}

function isOpen(loc, features) {
	console.log(loc.name + " is open!");
	var output = "<tr><td>" + loc.name + "</td><td>";
	for (var i = 0; i < features.length; i ++) { 
		output += "<i class='icon-" + features[i].icon + "'></i>";
	}
	output += "</td></tr>";
	$('#open_list').append(output);
}